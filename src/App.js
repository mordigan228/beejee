import React, { Component } from 'react';
import './App.css';
import {connect} from 'react-redux'
import Post from './Components/post'
import PostAdmin from './Components/postAdmin'
import Header from './Components/header/Header'
import {createUser, getData, switchPage} from "./redux/actions/actions";
import Footer from './Components/Footer'
import Login from "./Components/Login";

class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      url: this.props.url,
      count: 1
    }
  }
  componentDidMount() {
    this.props.getData(this.state.url)
  }
  render() {
    return (
      <div className="App">
        {this.props.page === 'main' ? (
            <div>
              <Header admin={this.props.user.admin}
                      switchPage={this.props.switchPage}
                      createUser={this.props.createUser}
              />
              {this.props.user.admin ? <PostAdmin/> : <Post />}
              <Footer />
            </div>
        ) : (<Login/>)}
      </div>
    );
  }
}
const mapState = (state) => {
  return {
    url: state.url,
    page: state.page,
    user: state.user
  }
}
const mapDispatch = (d) => {
  return {
    getData: e => d(getData(e)),
    createUser: e => d(createUser(e)),
    switchPage: e => d(switchPage(e))
  }
}

export default connect(mapState, mapDispatch)(App);
