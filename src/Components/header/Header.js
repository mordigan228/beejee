import React from 'react'
import './header.css'
import Form from "../Form";

const Header = (props) => {
    return(
        <header className="bg-black-90 flex w-100 ph3 pv3 ph4-m ph5-l">
            {props.admin ?
                <div className={'flex'}>
                    <span className={"white paddingr"}>Admin</span>
                    <a className="link dim dib white" onClick={()=>{
                        props.createUser({})
                    }
                    } href="#">Log out</a>
                </div>
                :
                <div className={'flex2'}>
                    <a onClick={()=>props.switchPage('login')} className="link dim dib white paddingr" href="#">Log in</a>
                    <Form/>
                </div>
            }
        </header>
    )
}
export default Header