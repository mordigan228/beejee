import React, {Component} from 'react'
import {connect} from 'react-redux'
import {createUser, switchPage} from "../../redux/actions/actions";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            loginFailed: false
        }
    }
    render(){
        console.log(this.state)
        return(
            <main className="pa4 black-80">
                <form method={'post'} onSubmit={e=>e.preventDefault()} className="measure center">
                    <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                        <legend className="f4 fw6 ph0 mh0">Log In</legend>
                        {this.state.loginFailed ? <p>Wrong username or password</p> : null}
                        <div className="mt3">
                            <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                            <input onChange={e=>this.setState({email:e.target.value})}
                                   className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                                   name="email-address" id="email-address" />
                        </div>
                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                            <input onChange={e=>this.setState({password:e.target.value})}
                                   className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100"
                                   type="password" name="password" id="password" />
                        </div>
                    </fieldset>
                    <div className="">
                        <input className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib"
                               onClick={()=>{
                                   if(this.state.email === 'admin' && this.state.password === '123'){
                                       this.props.createUser({
                                           email: this.state.email,
                                           password: this.state.password,
                                           admin: true
                                       })
                                       this.props.switchPage('main')
                                   }
                                   return this.setState({loginFailed: true})
                               }} value="Sign in" />
                    </div>
                </form>
            </main>

    )
    }
}

const mapState = state => {
    return {}
}

const mapDispatch = d => {
    return {
        createUser: e => d(createUser(e)),
        switchPage: e => d(switchPage(e))

    }
}

export default connect(mapState, mapDispatch)(Login)