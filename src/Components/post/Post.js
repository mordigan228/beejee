import React from 'react'

const Post = props => {
    return (
        <tbody>
            <tr id={props.id}>
                <td>{props.username}</td>
                <td>{props.email}</td>
                <td>{props.status}</td>
                <td>{props.text}</td>
                <td><img src={props.image}></img></td>
            </tr>
        </tbody>
    )
}
export default Post