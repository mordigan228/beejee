import React from 'react'
import Post from "./Post";
import './posts.css'
import {connect} from "react-redux";
import {sortField} from "../../redux/actions/actions";


class Posts extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        let {data, url, sortField} = this.props
        return (
            <table className={'containerTable'}>
                <thead>
                    <tr>
                        <th className={'pointer'}
                            onClick={()=>sortField(url, 'username')}>
                            Username
                        </th>
                        <th className={'pointer'}
                            onClick={()=>sortField(url, 'email')}>
                            Email
                        </th>
                        <th className={'pointer'}
                            onClick={()=>sortField(url, 'status')}>
                            Status
                        </th>
                        <th>Text</th>
                        <th>Image</th>
                    </tr>
                </thead>
                {data}
            </table>
            )
    }
}
const mapState = state => {
    return {
        data: state.data.map((i,j)=>{
            let {username, email, image_path, status, text, id} = i;
            return (<Post username={username} email={email} status={status} text={text} id={id} image={image_path} key={j} />)
        }),
        url: state.url
    }
}

const mapDispatch = d => {
    return {
        sortField: (url, e) => d(sortField(url, e))
    }
}

export default connect(mapState, mapDispatch)(Posts)