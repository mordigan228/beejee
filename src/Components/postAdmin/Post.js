import React from 'react'
const md5 = require('md5');

export default class Post extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            status: this.props.status,
            text: this.props.text
        };
    };
    render(){
        let {status, text} = this.state,
            {id, email, username} = this.props;
        let url = `https://uxcandy.com/~shapoval/test-task-backend/edit/${id}`
        return (
                    <form id={'editForm'} name={'edit'}
                          onSubmit={e=>{
                              let form = new FormData(document.getElementById('editForm'))
                              fetch(
                                  url, {
                                      method: 'post',
                                      mode: 'cors',
                                      body: [form, `signature=${md5(form)}`, "token=beejee"]
                                  }
                              ).then(console.log).catch(console.log)
                              e.preventDefault()
                          }}
                          id={id}
                          action={url}
                    >
                        <span>{username}</span>
                        <span>{email}</span>
                        <span><input placeholder={status} value={status}
                                   onChange={e=>this.setState({status: e.target.value})} name={'status'} /></span>
                        <span><input placeholder={text} value={text}
                                   onChange={e=>this.setState({text: e.target.value})} /></span>
                        <input value={'Submit'} type={'submit'}/>
                    </form>
        )
    }
}