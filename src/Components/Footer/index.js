import React, {Component} from 'react'
import {connect} from 'react-redux'
import {setPage} from "../../redux/actions/actions";
import './footer.css'

class Footer extends Component {
    constructor(props){
        super(props)
    }

    render(){
        let {numOfPages, url, setPage} = this.props;
        let currentPage = () => this.props.currentPage;
        let numberOfPages = Array.from({length: numOfPages}, (v, k) => k+1);
        const closePages = () => {
            if(numberOfPages.length > 24){
                if(currentPage() >= 10) {
                    return numberOfPages.slice(currentPage()-10, currentPage()+14)
                }
                return numberOfPages.slice(currentPage()-1, currentPage()+34)
            }
            return numberOfPages
        };
        let closestPages = closePages();
        let pages = closestPages ? closestPages.map((i,j)=>{
            let isActive = i === currentPage() ?
                "f6 link dim ph3 pv2 mb2 dib white bg-black" :
                "f6 link dim ba ph3 pv2 mb2 dib black"
            return <a href={'#'} key={j} onClick={()=>{
                setPage(url, i);
            }} className={isActive}>{i}</a>
        }) : <p>Loading...</p>
        return(
            <footer className={'position'}>
                <div className={'flex'}>
                    {pages}
                </div>
            </footer>
        )
    }
}

const mapState = state => {
    return {
        numOfPages: state.numOfPages,
        currentPage: state.currentPage,
        url: state.url
    }
}

const mapDispatch = d => {
    return {
        setPage: (url, page) => d(setPage(url, page))
    }
}

export default connect(mapState, mapDispatch)(Footer)