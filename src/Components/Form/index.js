import React, {Component} from 'react'
import {connect} from 'react-redux'
import './form.css'
import {getData} from "../../redux/actions/actions";

class Form extends Component{
    constructor(props){
        super(props);
        this.state={
            email: '',
            username: '',
            text: ''
        };
    }
    render(){
        let {getData, url} = this.props
        return(
            <div className={'flex'}>
                <form id={'submitForm'} name={'submitForm'}
                      className={'flex'}
                      onSubmit={(e)=>{
                          let form = new FormData(document.getElementById('submitForm'))
                          fetch(
                              'https://uxcandy.com/~shapoval/test-task-backend/create?developer=Aram',
                              {
                                  method: 'post',
                                  body: form,
                                  mode: 'cors'
                              }
                          ).then(console.log).catch(console.log)
                          e.preventDefault();
                          getData(url)
                      }}
                >
                    <input className={"input-reset ba b--black-20 pa2 mb2 db w-100"}
                           onChange={(e)=>this.setState({email: e.target.value})}
                           name={'email'}
                           type={'email'}
                           value={this.state.email}
                           placeholder={'Email'}
                    />
                    <input className={"input-reset ba b--black-20 pa2 mb2 db w-100"}
                           onChange={(e)=>this.setState({username: e.target.value})}
                           type={'text'}
                           value={this.state.username}
                           name={'username'}
                           placeholder={'Username'} />
                    <input className={"input-reset ba b--black-20 pa2 mb2 db w-100"}
                           placeholder={'Text'}
                           name={'text'}
                           onChange={(e)=>this.setState({text: e.target.value})}
                           value={this.state.text}
                    />
                    <button value={'Submit'}
                            className="f6 link dim ba ph3 pv2 mb2 dib dark-blue"
                            type={'submit'}
                    >Submit</button>
                </form>
            </div>
        )
    }
}
const mapState = state => {
    return {
        url: state.url
    }
}
const mapDispatch = d => {
    return {
        getData: e => d(getData(e))
    }
}
export default connect(mapState, mapDispatch)(Form)