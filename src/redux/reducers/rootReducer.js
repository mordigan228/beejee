import * as types from '../types/actionTypes'
const initialState = {
    url: 'https://uxcandy.com/~shapoval/test-task-backend/?developer=Aram&page=1&sort_field=status&sort_direction=desc',
    data: [],
    numOfPages: null,
    currentPage: 1,
    page: 'main',
    user: {}
};
export const rootReducer = (state=initialState, action) => {
    switch (action.type) {
        case types.CREATEUSER:
            return {
                ...state,
                user: action.payload
            }
        case types.GET_DATA:
            return {
                ...state,
                data: action.payload
            };
        case types.SETPAGESCOUNT:
            return {
                ...state,
                numOfPages: Math.ceil(action.payload/3)
            };
        case types.GETPAGE:
            return {
                ...state,
                currentPage: action.payload
            }
        case types.SWITCHPAGE:
            return {
                ...state,
                page: action.payload
            }
        case types.SETPAGE:
            return {
                ...state,
                url: action.payload.href
            }
        default:
            return state
    }
}