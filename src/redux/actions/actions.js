import * as types from '../types/actionTypes'

export const getData =  url => {
    return d => fetch(url)
        .then(data => data.json())
        .then(json => {
            d(setData(json.message.tasks))
            d(setPagesCount(json.message.total_task_count))
        })
        .catch(console.log)
}

export const setPage = (url, page) => {
    return d => {
        d(setPage1(url, page));
        let uri = new URL(url);
        uri.searchParams.set('page', page);
        d(getData(uri));
        d(getPage(uri));
    }
}

const setPagesCount = data => {
    return {
        type: types.SETPAGESCOUNT,
        payload: data
    }
}

const setPage1 = (url,page) => {
    let uri = new URL(url);
    uri.searchParams.set('page', page);
    return {
        type:types.SETPAGE,
        payload: uri
    }
}
export const sortField = (url, field) => {
    return d => {
        d(setSortField(url, field));
        let uri = new URL(url);
        uri.searchParams.set('sort_field', field);
        d(getData(uri));
        d(setAscDesc(uri))
    }
}

const setSortField = (url,field) => {
    let uri = new URL(url);
    uri.searchParams.set('sort_field', field);
    return {
        type:types.SETPAGE,
        payload: uri
    }
}
const setAscDesc = (url) => {
    let uri = new URL(url);
    let param = uri.searchParams.get('sort_direction');
    let value = param === 'asc' ? 'desc' : 'asc';
    uri.searchParams.set('sort_direction', value);
    return {
        type:types.SETPAGE,
        payload: uri
    }
}
export const getPage = (url) => {
    let uri = new URL(url);
    let page = uri.searchParams.get('page');
    return {
        type:types.GETPAGE,
        payload: page
    }
}

export const createUser = e => {
    return {
        type: types.CREATEUSER,
        payload: e
    }
}

export const switchPage = e => {
    return {
        type: types.SWITCHPAGE,
        payload: e
    }
}

const setData = (data) => {
    return {
        type: types.GET_DATA,
        payload: data
    }
}